#!/usr/bin/env python
"""
Welcome to inconsistentGlosses.py!
This script takes two arguments: a flex lexicon, and a directory containing flextexts. It finds inconsistent glossing in the flextexts, compares the glossing to the lexicon, and outputs a file for manual correction.

"""

import sys
import codecs
import os
try:
    import xml.etree.cElementTree as ET
except ImportError:
    import xml.etree.ElementTree as ET
from argparse import ArgumentParser

sys.stdout=codecs.getwriter('utf8')(sys.stdout)

def addMorph(morph,gloss,textdictionary,name):
	"""
	Adds morphs and glosses from the flextexts into the textdictionary. Also updates frequency and filenames.
	"""
	if morph in textdictionary:
		for glossinfo in textdictionary[morph]:
			if glossinfo[0] == gloss:
				glossinfo[1] += 1
				glossinfo.append(name)
				return
		textdictionary[morph].append([gloss,1,name])
		return
	else:
		textdictionary[morph] = [[gloss,1,name]]

def printInconsistencies(textdictionary,lexicon,variants,flextextCount,printfilenames,allophones):
	"""
	Prints any morphs with invalid glosses (taking into account variants), and morphs missing from the lexicon
	"""
	inconsistenciesCount = 0
	missingMorphCount = 0
	if printfilenames:
		print "\t".join(["Morph","Original gloss","Gloss to correct","Frequency","File(s)","In lexicon","Comments",])
	else:
		print "\t".join(["Morph","Original gloss","Gloss to correct","Frequency","In lexicon","Comments",])
	for morph in sorted(textdictionary.keys()):
		sortedlist = sorted(textdictionary[morph], key=lambda x: int(x[1]), reverse = True)
		includeinOutput = False
		if morph in lexicon:
			glosses = set(lexicon[morph])
			if morph in variants:
				relatedmorph = variants[morph]
				relatedglosses = set(lexicon[relatedmorph])
			for glossinfo in textdictionary[morph]: # Only including morphs if at least one of the glosses is not in the lexicon (variants are linked to their related headword's glosses) 
				gloss = glossinfo[0]
				if morph in variants:
					if gloss not in glosses and gloss not in relatedglosses:
						includeinOutput = True
				elif gloss not in glosses:
					includeinOutput = True
			if includeinOutput:
				print "%s" %(morph),
				inconsistenciesCount += 1
				for glossinfo in sortedlist:  
					gloss = glossinfo[0]
					count = glossinfo[1]
					filenamelist = set(glossinfo[2:])
					print "\t%s\t%s\t%s\t" %(gloss,gloss,count),
					# Extra information to assist the user
					if printfilenames: # Verbose option prints filenames
						if gloss not in glosses and ((morph not in variants) or (gloss not in relatedglosses)):
								print ", ".join(filenamelist) + "\t",
						else:
							print "\t",
					if gloss in glosses: # Is the gloss in the lexicon?
						print u'\u2713', # Tick of approval
					elif morph in variants: # Maybe it's a variant of something else?
						if gloss in relatedglosses: # Is the gloss valid for the related morph?
							print u'\u2713' + "\tVariant of %s" %(relatedmorph),
						elif len(relatedglosses) >=1 and len(glosses) >= 1: # Does it have its own glosses, as well as the related glosses?
							print u'\u2717' + "\tConsider: %s, or if it's a variant of %s: %s" %(", ".join(glosses), relatedmorph, ", ".join(relatedglosses)),
						elif len(relatedglosses) >=1: # It doesn't have its own glosses, maybe it only has related glosses?
							print u'\u2717' + "\tVariant of %s. Consider: %s" %(relatedmorph, ", ".join(relatedglosses)),
						else:
							print u'\u2717' + "\tVariant of %s. There is no gloss for %s or %s in the lexicon." %(relatedmorph,morph,relatedmorph), # It's a variant, but doesn't have any glosses
					elif len(glosses) >= 1: # Try these other glosses
						print u'\u2717' + "\tConsider: %s" %(", ".join(glosses)),
					else:
						print u'\u2717' + "\tThere is no gloss for %s in the lexicon." %(morph), # In the lexicon, but not a variant, and no glosses
					print
				print

		else:
			missingMorphCount += 1

	# Printing stats and missing morphs
	print >> sys.stderr,"%s flextext files analysed" %(flextextCount)
	print >> sys.stderr,"%s morphs with inconsistent glosses" %(inconsistenciesCount)
	print >> sys.stderr,"%s morphs missing from lexicon" %(missingMorphCount)
	print >> sys.stderr
	print >> sys.stderr,"Morph not in lexicon\tGloss in text\tFrequency\tComments",
	if printfilenames:
		print >> sys.stderr,"\tFile(s)"
	else:
		print >> sys.stderr
	for morph in sorted(textdictionary.keys()):
		if morph not in lexicon:
			sortedlist = sorted(textdictionary[morph], key=lambda x: int(x[1]), reverse = True)
			for glossinfo in sortedlist:
				gloss = glossinfo[0]
				count = glossinfo[1]
				filenamelist = set(glossinfo[2:])
				print >> sys.stderr, "%s\t%s\t%s"  %(morph, gloss, count),
				if morph in allophones:
					print >> sys.stderr, "\tAllophone: %s is citation form" %(allophones[morph]),
				else:
					print >> sys.stderr, "\t",
				if printfilenames:
					print >> sys.stderr, "\t%s" %(", ".join(filenamelist))
				else:
					print >> sys.stderr


def main():
	"""
	Reads inputs: a lexicon, flextexts
	"""
	parser = ArgumentParser(description = __doc__)
	parser.add_argument('lexicon', help = 'Flex lexicon')
	parser.add_argument('flexdir', help = 'Directory with interlinearised Flex texts')
	parser.add_argument('-v', '--verbose', action='store_true', help='Prints filenames where particular glosses are found')
	opts = parser.parse_args()
	printfilenames = opts.verbose

	# Reading lexicon
	tree = ET.parse(open(opts.lexicon, 'r'))
	root = tree.getroot()
	lexicon = {}
	lexiconIDs = {}
	relationIDs = {}
	variants = {}
	allophones = {}
	for entryelement in root.iter('entry'):
		variantmorph = ""
		senseID = ""
		relationID = ""
		entry = entryelement[0][0][0].text # The form of the morph is stored in <entry><lexical-unit><form><text>, always the first element.
		entryID = entryelement.attrib["id"]
		if entry not in lexicon:
				lexicon[entry] = []
				lexiconIDs[entry] = [entryID]
		for element in entryelement.iter('gloss'):
			gloss = element[0].text # The gloss is stored in <gloss><text> - the first and only element
			lexicon[entry].append(gloss)
		for element in entryelement.iter('sense'): # Making lists of entry/sense IDs in order to find related variants
			senseID = element.attrib["id"]
			lexiconIDs[entry].append(senseID)
		for element in entryelement.iter('relation'): # Making a dictionary of variants and their relation IDs
			if element.attrib["type"] == "_component-lexeme":
				if element.attrib["ref"]:
					relationID = element.attrib["ref"]
					relationIDs[entry] = relationID
		for element in entryelement.iter('variant'):
			allophone = ""
			for subelement in element.iter('form'):
				allophone = subelement[0].text
				if allophone:
					allophones[allophone] = entry
	for variantmorph in relationIDs: # Comparing relation IDs to the IDs of related morphs
		for relatedmorph in lexiconIDs:
			if relationIDs[variantmorph] in lexiconIDs[relatedmorph]:
				variants[variantmorph] = relatedmorph

	# Reading texts
	# The textdictionary looks like:
	# {
	# 	morph :	[
	#		[gloss, frequency, file, file, ...] ,
	#		[gloss, frequency, file, file, ...]
	#			]
	#	,
	# 	morph :	[
	#		[gloss, frequency, file, file, ...] ,
	#		[gloss, frequency, file, file, ...]
	#			]
	# }
	textdictionary = {}
	flextextCount = 0
	for root, dirs, files in os.walk(opts.flexdir, topdown=False):
		for name in files:
			fullpath = os.path.join(root,name)
			if name.endswith(".flextext"):
				flextextCount += 1
				tree = ET.parse(open(fullpath, 'r'))	# Setting up ElementTree
				textroot = tree.getroot()
				for element in textroot.iter('morph'):	# Looping through all the morph elements
					morph = ""
					gloss = ""
					for item in element:
						if item.attrib['type'] == 'cf':	# citation form
							morph = item.text
						if item.attrib['type'] == 'gls': # gloss
							if item.text:
								gloss = item.text.strip(".***") # Flex adds triple asterisks sometimes, but they can be ignored
							else:
								gloss = "MISSING GLOSS"
						if not gloss:	# Sometimes the gloss is empty (gls=""), sometimes there's no gloss at all.
							gloss = "MISSING GLOSS"

					if morph:
						addMorph(morph,gloss,textdictionary,name)
	
	printInconsistencies(textdictionary,lexicon,variants,flextextCount,printfilenames,allophones)

if __name__=="__main__":
	main()

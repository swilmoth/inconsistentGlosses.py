#!/usr/bin/env python
"""
Welcome to updateGlosses.py!
This is a companion script to inconsistentGlosses.py.
It corrects the glosses in your flextext files and makes new versions in ./UpdatedFlextexts
"""

import sys
import codecs
import os
try:
    import xml.etree.cElementTree as ET
except ImportError:
    import xml.etree.ElementTree as ET
from argparse import ArgumentParser

sys.stdout=codecs.getwriter('utf8')(sys.stdout)

def main():

	parser = ArgumentParser(description = __doc__)
	parser.add_argument('correctionfile', help = 'Corrected file from inconsistentGlosses.py')
	parser.add_argument('flexdir', help = 'Directory with interlinearised Flex texts')
	opts = parser.parse_args()

	# Reading corrected file
	corrections = {}
	morph = ""
	for line in codecs.open(opts.correctionfile, 'r', 'utf8'):
		oldgloss = ""
		newgloss = ""
		line = line.strip('\n')
		line = line.split('\t')
		if len(line) >= 2:
			if "Morph" not in line[0]: # Ignoring the header line
				if line[0]:
					morph = line[0].strip(' ')
				if line[1]:
					if line[1] != line[2]: # Only adding glosses that have been corrected
						oldgloss = line[1].strip(' ')
						newgloss = line[2].strip(' ')
						if morph not in corrections:
							corrections[morph] = [{oldgloss:newgloss}]
							# Sometimes Flex adds triple asterisks.
							# We're ignoring them in inconsistentGlosses.py
							# but they should still stay in the corrected version of the texts.
							corrections[morph].append({oldgloss+".***":newgloss}) 
						else:
							corrections[morph].append({oldgloss:newgloss})
							corrections[morph].append({oldgloss+".***":newgloss})
		else:
			morph = ""

	# Reading texts
	flextextCount = 0
	correctedFileCount = 0
	correctedGlosses = 0
	outDir = os.getcwd() + "/UpdatedFlextexts"
	for root, dirs, files in os.walk(opts.flexdir, topdown=False):
		for name in files:
			correctedFile = False 
			fullpath = os.path.join(root,name)
			if name.endswith(".flextext"):
				tree = ET.parse(open(fullpath, 'r'))	# Setting up ElementTree
				textroot = tree.getroot()
				for element in textroot.iter('morph'):	# Looping through all the morph elements
					morph = ""
					textgloss = ""
					for item in element:
						if item.attrib['type'] == 'cf':	# citation form
							if item.text in corrections:
								morph = item.text
							else:
								break
						if item.attrib['type'] == 'gls': # gloss
							hasGloss = True
							if item.text:
								textgloss = item.text
							else:
								textgloss = "MISSING GLOSS"
						if not textgloss:	# Sometimes the gloss element doesn't have any text, sometimes there's no gloss at all.
							hasGloss = False
							textgloss = "MISSING GLOSS"
					
					if morph in corrections:
						for oldglossdict in corrections[morph]: # Setting up the newgloss variable
							for key in oldglossdict:
								if textgloss == key: # There are two versions of each oldgloss, with and without asterisks
									oldgloss = key
									newgloss = oldglossdict[key]
									if oldgloss.endswith(".***"):
										newgloss = newgloss + ".***"
									if newgloss == "MISSING GLOSS":
										newgloss == ""
						for item in element: # Changing the gloss element
							if item.attrib['type'] == 'gls':
								if textgloss == oldgloss:
									correctedFile = True
									correctedGlosses += 1
									item.text = str(newgloss)
						if not hasGloss: # Adding a gloss element if it was missing.
							newelement = ET.SubElement(element, "item")
							newelement.attrib = {"lang":"en","type":"gls"}
							newelement.text = newgloss
			
				# Printing the new tree
				if correctedFile:
					if not os.path.exists(outDir):
						os.makedirs(outDir)
					
					newFile = outDir + "/" + name.replace(".flextext", ".updated.flextext")
					tree.write(newFile, encoding="UTF-8")
					correctedFileCount += 1

	if correctedFile:				
		if correctedGlosses ==1:
			print >> sys.stderr, "1 gloss corrected ",
		else:
			print >> sys.stderr, "%s glosses corrected" %(correctedGlosses),
		if correctedFileCount == 1:
			print >> sys.stderr, "in 1 flextext file."
		else:
			print >> sys.stderr, "in %s flextext files." %(correctedFileCount)
	else:
			print >> sys.stderr, "No incorrect glosses found."

if __name__=="__main__":
	main()
